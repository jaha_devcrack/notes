Heroku lets you deploy, run and manage applications written in ruby, Node.js, Java, Python, Clojure, Scala, Go and PHP.


Dependency mechanism vary across languages: in Ruby you use a Gemfile, in Python a requirements.txt, in Node.js a package.json,
in java a pom.xml and so on.


The sources code for you application, together with the dependency file, should provide enough information for the Heroku platform to build your application, and produce something that can be executed.

# Deploy

## The Procfile file 

Procfile is a file that contains information that declare explicitly what can be executed. Each line declares a process type a named command that can be executed against your built application. For example:

```bash
web: java -jar lib/foobar.jar $PORT
queue: java -jar lib/queue-processor.jar
```

This file declares a web process type and provides the command that needs to be executedin order to run it. 
It also declares a queue process type, and its corresponding command.

## Deploying applications 

The heroku platform uses git as the primary means for deploying applications(there are other ways for this).

When you create an application on Heroku, it associates a new Git remote, typically called named ```heroku```, with the local Git repository for you application. 

As a result, deploying code is just the familiar ```git push```, but to the ```heroku``` instead:
```git push heroku master```


## Building applications

When Heroku recieves the application source, it initiates a build of the source application. The build mechanism is typically language specific, but follows the same pattern.

The source code for you application, together with the fetched dependencies and output of the build phase such as generated assets or compiled code, as well the language and the framework are assembled into a [slug](https://devcenter.heroku.com/articles/slug-compiler).

A **slug** is a bundle of your source, fetched dependencies, the language runtime, and compiled/generated output of the build system - read for the execution.

The slugs contain your compiled, assembled application redy to run together with the instructions in the **procfile**.

## Running applications on dynos.

Heroku executes applications by running a command you specified in the Procfile, on a dyno.

Think of a running dyno as a lighweight, secure, virtualized Unix container that contains your application slug in its file system.


# Runtime


**Dynos** are isolated, virtualized Unix containers provide the environment required to run an application.

**dyno formation** is the total number of currently executing dynos, divided betwen the various process types you have scaled.

To understand what is executing, just need to know what dynos are running which process types:

```bash
$ heroku ps
== web: 'java lib/foobar.jar $PORT'
web.1: up 2013/02/07 18:59:17 (~ 13m ago)
web.2: up 2013/02/07 18:52:08 (~ 20m ago)
web.3: up 2013/02/07 18:31:14 (~ 41m ago)

== queue: `java lib/queue-processor.jar`
queue.1: up 2013/02/07 18:40:48 (~ 32m ago)
queue.2: up 2013/02/07 18:40:48 (~ 32m ago) 


```

In this example the application is well architected to allow for the independent scaling of web and queue worker dynos. 


## Config Vars

Configuration vars includes backing services such as databases, credentials, or environment variables that provide some specific information to your application.


**Config vars** basically contain customizable configuration data that can be changed independently of your source code. The configuration is exposed to a running application via environment variables.

All dynos in applicattion will have access to exact same set of config vars at runtime.

## Releases

The combination of slug and configuration is called release.

**Releases* are an append only ledger of slugs and config vars.

Use the ```heroku releases``` command to see the audit trail of relases deploys.

Every time you deploy a new version of an application, a new slug is created and release is generated.

As Heroku contains store of the previous releases of your applicatio, its very easy to rollback and deploy a previous release.

```bash
heroku releases:rollback v102
Rolling back demoapp... done, v102
heroku releases
== demoapp Releases
v104 Rollback to v102 jon@heroku.com   2013/01/31 14:11:33 (~15s ago)
v103 Deploy 582fc95   jon@heroku.com   2013/01/31 12:15:35
v102 Deploy 990d916   jon@heroku.com   2013/01/31 12:01:12
```

## Dyno manager

The Dyno manager is responsible for keeping dynos running. For example, dynos are clycled at least once per day, or whenever the dyno manager detects a fault in the running application.

A great characteristic is **One--off dynos** that can be run with their input/output attached to your local terminal. These can also be used to carry out admin task that modify the state of shared resources, for example database configuration, 


**One--off dynos** are temporary dynos that can run with their input/output attached to your local terminal. They're loaded with your **lastest release**.

Each dyno gets its own ephemeral file system with a fresh copy of the most recen release, this means that the changes of the one-off dyno file system on the dyno are not propagated to other dynos and are not persisted across deploys and dyno restarts. A better and more scalable approach is to use a shared resource such as database or queue.

For create a **One--off dyno** run the ```heroku run bash``` command.

## Add--ons

Add-ons are provided as services by Heroku and third parties that enhance the services of the applications  such as databases, queueing & caching systems, storage, email services and more. The add-on is attached to your application.

For example here is how to add Heroku Redis backing store add-on to an application.

```heroku addons:create heroku-redis:hobby-dev```

THe add-on service provider is responsible for the service and the interface to your application is often provided through a config var. 
In this example a ```REDIS_URL``` will be automatically added to your application when you provision the add-on. 
So the code for connects to the service through the URL, is as follows:

```
uri = URI.parse(ENV["REDIS_URL"])
REDIS = Redis.new(:host => uri.host, :port => uri.port, :password => uri.password)
```

Add-ons are associated with an application, much like config vars.


## Logging and monitoring 

Heroku treats logs as streams of time-stamped events, and collates the stream of logs produced from all of the processes running in all dynos, and the Heroku platform components, into the Logplex - a high-performance, real-time system for log delivery.


It’s easy to examine the logs across all the platform components and dynos:

```
heroku logs
2013-02-11T15:19:10+00:00 heroku[router]: at=info method=GET path=/articles/custom-domains host=mydemoapp.heroku.com fwd=74.58.173.188 dyno=web.1 queue=0 wait=0ms connect=0ms service=1452ms status=200 bytes=5783
2013-02-11T15:19:10+00:00 app[web.2]: Started GET "/" for 1.169.38.175 at 2013-02-11 15:19:10 +0000
2013-02-11T15:19:10+00:00 app[web.1]: Started GET "/" for 2.161.132.15 at 2013-02-11 15:20:10 +0000
```


## HTTP routing 

The dynos taht run process types named ```web``` are different in one way from all other dynos they receive HTTP traffic. Heroku's [HTTP routers](https://devcenter.heroku.com/articles/http-routing) distributed incoming requests for your application across your running web dynos.

So scalling an app's capacity to handle web traffic involves scaling the number of web dynos:

```
heroku ps:scale web+5
```

A random selection algorithm is used for HTTP request load balancing across web dynos and this routing handles both HTTP and HTTPS. It also supports multiple simultaneous connections, as well as timeout handling.





[How Heroku Works](https://devcenter.heroku.com/articles/how-heroku-works)